// AJAX - Асинхронный JavaScript и XML. Это набор методов веб-разработки,  которые позволяют веб-приложениям работать асинхронно —  обрабатывать любые запросы к серверу в фоновом режиме. В результате любое веб-приложение, использующее AJAX,  может отправлять и извлекать данные с сервера без необходимости перезагрузки всей страницы.




const root = document.getElementById("root");
const url = "https://ajax.test-danit.com/api/swapi/films";

fetch(url)
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    data.map(({ name, episodeId, openingCrawl, characters }) => {
      addEpisodes(name, episodeId, openingCrawl, characters);
      const promises = characters.map((link) =>
        fetch(link).then((response) => response.json())
      );
      Promise.all(promises).then((data) => {
        data.forEach((data) => {
          addCharacters(data, episodeId);
        });
      });
    });
  });

const addEpisodes = function (name, episodeId, openingCrawl) {
  const listElement = document.createElement("div");
  listElement.insertAdjacentHTML("afterbegin", [
    `<h2>${name}</h2><ul data-id="${episodeId}"></ul><h3>Episode ${episodeId}
    </h3><span>${openingCrawl}</span>`,
  ]);
  root.append(listElement);
};

const addCharacters = function (data, episodeId) {
  const listСharacter = document.createElement("li");
  listСharacter.insertAdjacentHTML("afterbegin", `<li>${data.name}</li>`);
  document.querySelector(`[data-id="${episodeId}"]`).append(listСharacter);
};
